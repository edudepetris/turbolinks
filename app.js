const express = require('express')
const app = express()

// Configuration
app.set('view engine', 'html');
app.engine('html', require('hbs').__express);
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));

// Routes
app.get('/', (req, res) => res.render('index'))
app.get('/about', (req, res) => res.render('about'))
app.get('/news', (req, res) => res.render('news'))
app.get('/contact', (req, res) => res.render('contact'))

app.listen(3000, () => console.log('Example app listen on port 3000!'))